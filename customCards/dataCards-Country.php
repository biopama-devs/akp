<?php

/* 
Anatomy of a Data Card
'cardId' => [ required to identify and use the card in JS
  'name' => '',
  'description' => '', 
  'tags' => [], //! list all of the topic taxonomy tags as strings in an array eg: [611, 612]
  'descriptionLong' => '', //not really used, but it could be!
  'customTop' => '', //Puts a generic div at the top for more custom content - for injecting via JS
  'chart' => BOOL,
  'table' => BOOL, 
  'layers' => [[
    'name' => '', //title of the layer
	'id' => '', //ID to uniquely manage this layer in the map
	'buttonTitle' => '', //if there's more then one layer in the array this will be in the title of the generated button
	'type' => '', //Raster or Vector
	'scheme' => '', //if raster - xyz, tms... other? 
	'url' => '', //the URL - it it needs an argument, that's managed in JS
	'customArgs' => BOOL, //setting this to true will prevent the layer from being rendered normally. Instead it will need to be called specifically so we can attach the custom arguments.
	'description' => '', 
	'legend' => [
	  'type' => 'choropleth', //only choropleth or gradient
	  'classes' => [
	  	[
		  'color' => '#fff', //supports any color type (hex, rgb, etc)
		  'label' => '', //it's what is written under the corresponding color 
		  'class' => 'fires_1d', //optional - todo add interaction
		]
	  ]
	], 
  ]],
  'customBottom' => '', //Puts a generic div at the bottom for more custom content - for injecting via JS
  'link' => '', //!mandatory
  'linkIcon' => '', //todo - add optional icon for reference link
  'linkText' => '', //if null, it will display default text
],
 */

$DataCardsBiodiversityAndForest = [ 

	'TerrestrialEcoregions' => [ 
		'name' => 'Terrestrial ecoregions',
		'description' => 'List of terrestrial ecoregions in country and protected areas coverage statistics.', 
		'tags' => [612, 618, 611, 615], 
		'policies' => [24356], 
		'rcoes' => [928], 
		'buttonGroup' => NULL, 
		'chart' => NULL,
		'table' => TRUE, 
		'layer' => NULL,
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Terrestrial+Coverage+by+Protected+Areas', 
    ],
	'MarineEcoregions' => [
		'name' => 'Marine ecoregions',
		'description' => 'List of marine ecoregions in country and protected areas coverage statistics.', 
		'tags' => [614, 612, 618, 611, 615], 
		'descriptionLong' => NULL, 
		'buttonGroup' => NULL, 
		'chart' => NULL,
		'table' => TRUE, 
		'layer' => NULL,
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Marine+Coverage+by+Protected+Areas',  
    ],
	'InlandSurfaceWater' => [ 
		'name' => 'Inland surface water',
		'description' => 'Areas of permanent and seasonal surface inland water and their changes over time (1984 - 2020) are expressed in km<sup>2</sup> and percentages.', 
		'tags' => [616, 612, 618, 611, 615], 
		'descriptionLong' => NULL, 
		'buttonGroup' => NULL, 
		'chart' => TRUE,
		'table' => TRUE, 
		'layer' => NULL,
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Inland+Surface+Water', 
    ],

	'LandDegradation' => [
		'name' => 'Land degradation',
		'tags' => [588, 629, 612, 638, 618, 639, 611, 615, 628], 
		'description' => 'Virtual elevation profile of the country providing minimum, maximum, median, mean elevation values in meters.', 
		'chart' => TRUE,
		'table' => TRUE, 
		'layer' => NULL,
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Land+Productivity', 
    ], 
	'LandFragmentation' => [
		'name' => 'Land fragmentation',
		'description' => 'Virtual elevation profile of the country providing minimum, maximum, median, mean elevation values in meters.', 
		'tags' => [612, 618, 611, 615], 
		'descriptionLong' => NULL, 
		'buttonGroup' => NULL, 
		'chart' => TRUE,
		'table' => TRUE, 
		'layer' => NULL,
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Land+Fragmentation', 
    ],
    'BelowGroundCarbon' => [
		'name' => 'Below ground carbon',
		'description' => 'Country statistics for the amount of below ground carbon', 
		'tags' => [612, 618, 611, 615], 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Below+Ground+Carbon', 
		'linkText' => NULL, 
    ],
	'SoilOrganicCarbon' => [
		'name' => 'Soil organic carbon',
		'description' => 'Country statistics for the amount of soil organic carbon (0-30 cm depth)',
		'tags' => [612, 618, 611, 615], 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL, 
		'link' => 'http://dopa.jrc.ec.europa.eu/sites/default/files/DOPA%20Factsheet%20J1%20Soil%20Carbon_0.pdf', 
		'linkText' => NULL, 
    ],
	'AboveGroundCarbon' => [
		'name' => 'Above ground carbon',
		'description' => 'Country statistics for above ground carbon.', 
		'tags' => [612, 618, 611, 615], 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Above+Ground+Carbon', 
		'linkText' => NULL, 
    ],
	'LitterCarbon' => [
		'name' => 'Litter carbon',
		'description' => 'Country statistics for litter carbon.', 
		'tags' => [612, 618, 611, 615], 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Above+Ground+Carbon', 
		'linkText' => NULL, 
    ],
	'DeadWoodCarbon' => [
		'name' => 'Dead wood carbon',
		'description' => 'Country statistics for dead wood carbon.', 
		'tags' => [612, 618, 611, 615], 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Above+Ground+Carbon', 
		'linkText' => NULL, 
    ],
	'TotalCarbon' => [
		'name' => 'Total carbon',
		'description' => 'Country statistics for the amount of total carbon (below ground C +  organic soil C + above ground C).', 
		'tags' => [612, 618, 611, 615], 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Carbon', 
		'linkText' => NULL,
    ],
	'CopernicusGLC' => [
		'name' => 'Copernicus Global Land Cover 2015',
		'description' => 'Using the first aggregation level, the land cover classes are provided for this country for the year 2015 km<sup>2</sup> and %.', 
		'tags' => [612, 618, 611, 615, 608, 629], 
		'buttonGroup' => NULL,
		'chart' => TRUE,
		'table' => TRUE, 
		'layer' => NULL, 
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Land+Cover', 
		'linkText' => NULL, 
    ],
	'EsaLC' => [
		'name' => 'ESA Land Cover change 1995-2020',
		'description' => 'The land cover class change for this country from the years 1995 to 2020 in km<sup>2</sup>', 
		'tags' => [612, 618, 611, 615, 608, 629], 
		'buttonGroup' => NULL,
		'chart' => TRUE,
		'table' => FALSE, 
		'layer' => NULL,
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Land+Cover', 
		'linkText' => NULL, 
    ],
	'SpeciesAnimalPlantNumbers' => [
		'name' => 'Reported number of animal and plant species',
		'description' => 'The number of animal and plant species as reported by the IUCN Red List of Threatened Species',
		'tags' => [612, 618, 611, 615, 608, 629], 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,
		'customBottom' => '', //todo 
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species', 
		'linkText' => NULL, 
    ],
	'SpeciesThreatNumbers' => [
		'name' => 'Reported number of threatened amphibians, birds and mammals',
		'description' => 'The number of threatened, endemic and assessed amphibian, bird and mammals species as reported by the IUCN Red List of Threatened Species',
		'tags' => [612, 618, 611, 615, 608, 629], 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,
		'customBottom' => '', //todo 
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species', 
		'linkText' => NULL, 
    ],
	'SpeciesThreatEndemicNumbers' => [
		'name' => 'Reported endemic and threatened endemic vertebrates in the country',
		'description' => 'The number of threatened and endemic vertebrates as reported by the IUCN Red List of Threatened Species',
		'tags' => [612, 618, 611, 615, 608, 629], 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,
		'customBottom' => '', //todo 
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species', 
		'linkText' => NULL, 
    ], 
	'SpeciesComputed' => [
		'name' => 'Threatened and near threatened species',
		'description' => 'The species list in country is computed from the species ranges recorded in the IUCN Red List of Threatened Species', 
		'tags' => [612, 618, 611, 615, 608, 629], 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => FALSE,  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species', 
		'linkText' => NULL, 
    ],

	'NumPas' => [
		'name' => 'Number of protected areas',
		'description' => 'Number of Terrestrial, Marine and Coastal protected areas in country.', 
		'tags' => [612, 618, 611, 615, 608, 629], 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,  
		'link' => NULL, 
		'linkText' => NULL, 
    ],
	'ListPas' => [
		'name' => 'List of protected areas',
		'cardId' => 'list-pas', 
		'description' => 'List of Terrestrial, Marine and Coastal protected areas in the country.', 
		'tags' => [612, 618, 611, 615, 608, 629], 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,  
		'link' => NULL, 
		'linkText' => NULL, 
    ],
	'KBA' => [
		'name' => 'Key Biodiversity Areas',
		'description' => 'Number and protection of Key Biodiversity Areas.', 
		'tags' => [612, 618, 611, 615, 608, 629], 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Key+Biodiversity+Areas', 
		'linkText' => NULL, 
    ],
	'EstPaSpeciesThreat' => [
		'name' => 'Estimated number of threatened and near threatened species for protected areas',
		'cardId' => 'est-species-threat-pas', 
		'description' => 'Estimated number of threatened and near threatened species for protected areas in country as extracted from the range maps of the species documented by the IUCN.', 
		'tags' => [612, 618, 611, 615, 608, 629], 
		'buttonGroup' => NULL,
		'chart' => TRUE,
		'table' => TRUE, 
		'layer' => NULL,  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species', 
		'linkText' => NULL, 
    ],
	'PaHabitatDiversity10' => [
		'name' => 'Habitat diversity in protected areas',
		'cardId' => 'hab-diversity-pas-10', 
		'description' => 'Country ranking of the protected areas according to the diversity of their habitats.', 
		'tags' => [612, 618, 611, 615, 608, 629], 
		'buttonGroup' => NULL,
		'chart' => TRUE,
		'table' => TRUE, 
		'layer' => NULL,  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Habitat', 
		'linkText' => NULL, 
    ],
	'PasPressures10' => [
		'name' => 'List of protected areas and associated pressures',
		'cardId' => 'list-pas-10-pressures', 
		'description' => 'List of protected areas ≥ 1 km<sup>2</sup> and associated pressures..', 
		'tags' => [612, 618, 611, 615, 608, 629], 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Pressure', 
		'linkText' => NULL, 
    ],
];

$DataCardsWater = [ 
	'ElevationProfile' => [
		'name' => 'Elevation profile',
		'description' => 'Virtual elevation profile of the country providing minimum, maximum, median, mean elevation values in meters.', 
		'tags' => [611, 612], 
		'chart' => TRUE,
		'table' => TRUE, 
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Elevation', 
    ],
];

$DataCardsFoodSecurityAndAgroecology = [ 
];

$DataCardsEnergy = [
];

$DataCardsClimateAndDisasterResilience = [
];

$DataCardsOceans = [
];


?>